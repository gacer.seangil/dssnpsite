namespace ElementInteractivity {
  export function initHeaderInteractivity() {
    console.log("FIRING: Adding header interactivity.");
    const headerhbg = document.getElementById("navi-hbg");
    const headerbottom = document.getElementsByTagName("header")[0].getElementsByClassName("bottom")[0];
    if (!headerhbg) {console.error("Cannot find header hamburger button!"); return;} else {console.log(headerhbg);}
    if (!headerbottom) {console.error("Cannot find header dropdown!"); return;} else {console.log(headerbottom);}
    headerhbg.addEventListener("click", () => {
      headerbottom.classList.toggle("expand");
    })
  }
}

ElementInteractivity.initHeaderInteractivity();